<?php
include 'db.php';

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $sql = "SELECT appointments.appointment_id, CONCAT(patients.first_name, ' ', patients.last_name) AS patient_name, procedures.procedure_name, dentists.first_name AS dentist_name, appointments.appointment_date, appointments.appointment_time, patients.email, patients.contact_number, patients.age, patients.sex, procedures.cost
        FROM appointments
        JOIN patients ON appointments.patient_id = patients.patient_id
        JOIN procedures ON appointments.procedure_id = procedures.procedure_id
        JOIN dentists ON appointments.dentist_id = dentists.dentist_id
        WHERE appointments.appointment_id = $id";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        echo "<form action='edit.php' method='POST'>
        <!-- Appointment Details -->
        <h3>Appointment Details</h3>
        <label for='patient_name'>Patient Name:</label>
        <input type='text' id='patient_name' name='patient_name' value='". $row['patient_name']."' disabled>
        <br>
        <label for='procedure_name'>Procedure:</label>
        <input type='text' id='procedure_name' name='procedure_name' value='". $row['procedure_name'] ."' disabled>
        <br>
        <label for='dentist_name'>Dentist Name:</label>
        <input type='text' id='dentist_name' name='dentist_name' value='". $row['dentist_name']."' disabled>
        <br>
        <label for='appointment_date'>Date:</label>
        <input type='date' id='appointment_date' name='appointment_date' value='". $row['appointment_date']."' \>
        <br>
        <label for='appointment_time'>Time:</label>
        <input type='time' id='appointment_time' name='appointment_time' value='". $row['appointment_time']."' \>
        <input type='hidden' name='id' value='". $row['appointment_id']."' \>
        <br><br>

        <input type='submit' value='Update Appointment'>
    </form>";
    } else {
        echo "Reservation not found.";
    }
} elseif($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_POST['id'];
    $appointment_date = $_POST['appointment_date'];
    $appointment_time = $_POST['appointment_time'];

    $updateSql = "UPDATE appointments SET appointment_date = '$appointment_date', appointment_time = '$appointment_time'  WHERE appointment_id = $id";

    if($conn->query($updateSql) === TRUE){
        header("Location: set_appointment.php");
    } else {
        echo "Error updating reservation: " . $conn->error;
    }
} else {
    echo "Invalid Request";
}
$conn->close();

?>