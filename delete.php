<?php
include 'db.php';

session_start();

// Check if the 'username' session variable is not set
if (!isset($_SESSION['username'])) {
   // User is not logged in, redirect to the login page
   header("Location: login.php");
   exit(); // Ensure that the script stops executing after redirection
}


$id = $_GET['id'];

$sql = "DELETE from appointments WHERE appointment_id = $id";
    if($conn->query($sql) === TRUE){
        header("Location: set_appointment.php");
    } 
    else{
        echo "Error deleting reservation: " . $conn->error;
    }
$conn->close();
?>