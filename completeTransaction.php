<?php

include 'db.php';

$sql = "SELECT complete.complete_id, CONCAT(patients.first_name, ' ', patients.last_name) AS patient_name, procedures.procedure_name, dentists.first_name AS dentist_name, complete.appointment_date, complete.appointment_time, patients.email, patients.contact_number, patients.age, patients.sex, procedures.cost
        FROM complete
        JOIN patients ON complete.patient_id = patients.patient_id
        JOIN procedures ON complete.procedure_id = procedures.procedure_id
        JOIN dentists ON complete.dentist_id = dentists.dentist_id";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table>
            <tr>
                <th colspan='6'>Patient Information</th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Procedure</th>
                <th>Cost</th>
                <th>Dentist Name</th>
                <th>Date</th>
                <th>Time</th>
            </tr>";

    while ($row = $result->fetch_assoc()) {
        echo "<tr>
                <td>" . $row["patient_name"] . "</td>
                <td>" . $row["procedure_name"] . "</td>
                <td>₱ " . $row["cost"] . "</td>
                <td>" . $row["dentist_name"] . "</td>
                <td>" . $row["appointment_date"] . "</td>
                <td>" . $row["appointment_time"] . "</td>
              </tr>";
    }
    echo "</table>";
}
else{
    echo "<table>
            <tr>
                <th colspan='6'>Patient Information</th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Procedure</th>
                <th>Cost</th>
                <th>Dentist Name</th>
                <th>Date</th>
                <th>Time</th>
            </tr>
            </table>";
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    table, th, td{
        border: solid 1px black;
        border-collapse: collapse;
        padding: 10px;
    }
</style>
    </head>
    <body>
</body>
</html>