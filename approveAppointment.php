<?php

include 'db.php';

$error="";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $patient_id = $_POST['patient'];
    $dentist_id = $_POST['dentist'];
    $procedure_id = $_POST['procedure'];
    $appointment_date = $_POST['appointment_date'];
    $appointment_time = $_POST['appointment_time'];

    $sql = "SELECT * FROM appointments WHERE appointment_date = '$appointment_date' AND appointment_time = '$appointment_time' AND dentist_id = '$dentist_id'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Slot is already booked, inform the patient
        $error = "The selected slot is not available. Please choose another time.";
    } else {
    $sql = "INSERT INTO appointments (appointment_date, appointment_time, patient_id, dentist_id, procedure_id)
            VALUES ('$appointment_date', '$appointment_time', '$patient_id', '$dentist_id', '$procedure_id')";

    if ($conn->query($sql) === TRUE) {
        header("Location: set_appointment.php");
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
}

$conn->close();
?>
