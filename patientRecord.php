<?php

include 'db.php';

$search = "";
$date = "";

if(isset($_GET['search'])){
    $search = $_GET['search'];
}

if(isset($_GET['date'])){
    $date = $_GET['date'];
}

$sql = "SELECT appointments.appointment_id, CONCAT(patients.first_name, ' ', patients.last_name) AS patient_name, procedures.procedure_name, dentists.first_name AS dentist_name, appointments.appointment_date, appointments.appointment_time, procedures.cost
        FROM appointments
        JOIN patients ON appointments.patient_id = patients.patient_id
        JOIN procedures ON appointments.procedure_id = procedures.procedure_id
        JOIN dentists ON appointments.dentist_id = dentists.dentist_id
        WHERE (CONCAT(patients.first_name, ' ', patients.last_name) LIKE '%$search%' OR CONCAT(patients.first_name, ' ', patients.last_name) = '')
        AND (DATE(appointments.appointment_date) = '$date' OR '$date' = '')";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table>
            <tr>
                <th colspan='6'>Appointment Details</th>
                <th rowspan='2'>Action</th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Procedure</th>
                <th>Cost</th>
                <th>Dentist Name</th>
                <th>Date</th>
                <th>Time</th>
            </tr>";

    while ($row = $result->fetch_assoc()) {
        echo "<tr>
                <td>" . $row["patient_name"] . "</td>
                <td>" . $row["procedure_name"] . "</td>
                <td>₱ " . $row["cost"] . "</td>
                <td>" . $row["dentist_name"] . "</td>
                <td>" . $row["appointment_date"] . "</td>
                <td>" . $row["appointment_time"] . "</td>
                <td>
                    <a href='edit.php?id=" . $row["appointment_id"] . "'>Reschedule</a> | 
                    <a href='delete.php?id=" . $row["appointment_id"] . "'>Cancel Appointment</a>
                    <a href='complete.php?id=" . $row["appointment_id"] . "'>Complete</a>
                </td>
              </tr>";
    }
    echo "</table>";
}
else{
    echo "<table>
            <tr>
                <th colspan='6'>Appointment Details</th>
                <th rowspan='2'>Action</th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Procedure</th>
                <th>Cost</th>
                <th>Dentist Name</th>
                <th>Date</th>
                <th>Time</th>
            </tr>
            </table>";
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    table, th, td{
        border: solid 1px black;
        border-collapse: collapse;
        padding: 10px;
    }
</style>
    </head>
    <body>
</body>
</html>