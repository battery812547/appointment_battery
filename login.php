<?php
include 'db.php';

session_start();

$message="Input username and password to login";

if($_SERVER['REQUEST_METHOD'] == "POST"){
    $username = $_POST['username'];
    $password = $_POST['password'];
}

    $sql = "SELECT * FROM users WHERE username='$username' AND password='$password'";
    $result = $conn->query($sql);

    if($result -> num_rows > 0){
        $_SESSION['username'] = $username; // Set a session variable for verified user
        header("Location: set_appointment.php"); // Redirect to the reservation system
        exit(); // Ensures that the script stops executing after redirection
    }
    else{
        $message = "<h2 class='error'>Invalid username or password</h2>";
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <style>
        .error{
            color: maroon;
        }
    </style>
    <script>
        function togglePassword() {
            var passwordField = document.getElementById("password");
            if (passwordField.type === "password") {
                passwordField.type = "text";
            } else {
                passwordField.type = "password";
            }
        }
    </script>
</head>

<body>
    <h2>Login</h2>
    <form action="login.php" method="POST">
        <div>
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" required>
        </div>
        <div>
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" required>
            <span onclick="togglePassword()">SEE</span><br><br>
        </div>
        <button type="submit">Login</button>
        <?php echo $message; ?>
    </form>
</body>

</html>