<?php
include 'db.php';
session_start();

if(!isset($_SESSION['username'])){
    header("Location: login.php");
    exit(); // Ensures that the script stops executing after redirection
}


echo "Apostol Dental Cosmetic Center || " . $_SESSION['username'] . " | <a href='logout.php'> Logout </a>" ;
echo "<br><hr>";


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Appointment System</title>
       
    </head>
    <body>
    <h1>Patient Records</h1>
    <a href='addPatient.php'> Add Patient </a>
        <?php
        include 'patientInfo.php';
        ?>

    <hr>
    <h1>Appointment System</h1>

    <form action="approveAppointment.php" method='POST'>
    <label for="patient">Select Patient:</label>
        <select name="patient" id="patient" required>
        <option value = "" selected hidden >Select Patient</option>
            <!-- Fetch and display patient options from the patients table -->
            <?php
                include 'db.php';
                $sql = "SELECT * FROM patients";
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc()) {
                    echo "<option value='" . $row['patient_id'] . "'>" . $row['first_name'] . " " . $row['last_name'] . "</option>";
                }
            ?>
        </select><br><br>

        <label for="dentist">Select Dentist:</label>
        <select name="dentist" id="dentist" required>
        <option value = "" selected hidden >Select Dentist</option>
            <!-- Fetch and display dentist options from the dentists table -->
            <?php
                $sql = "SELECT * FROM dentists";
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc()) {
                    echo "<option value='" . $row['dentist_id'] . "'>" . $row['first_name'] . " " . $row['last_name'] . "</option>";
                }
            ?>
        </select>

        <label for="procedure">Select Procedure:</label>
        <select name="procedure" id="procedure" required>
        <option value = "" selected hidden >Select Procedure</option>
            <!-- Fetch and display procedure options from the procedures table -->
            <?php
                $sql = "SELECT * FROM procedures";
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc()) {
                    echo "<option value='" . $row['procedure_id'] . "'>" . $row['procedure_name'] . "</option>";
                }
            ?>
        </select><br><br>

        <label for="appointment_date">Appointment Date:</label>
        <input type="date" id="appointment_date" name="appointment_date" min="<?php echo date('Y-m-d'); ?>" required>
        <label for="appointment_time">Appointment Time:</label>
        <input type="time" id="meeting-time" name="appointment_time" required>
        <script>
            // Get the current time in 12-hour format with AM/PM
            var currentTime = new Date().toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true });

            // Extract AM/PM from the time
            var ampm = currentTime.split(' ')[1];

            // Set the value of the time input
            document.getElementById("meeting-time").value = currentTime;

            // Display AM or PM
            document.getElementById("ampm").textContent = ampm;
        </script>

        <br><br>
        <input type="submit" value="Set Appointment">
    </form>

    <?php
        include 'approveAppointment.php';
        ?>
    <br><br>
    <hr>
    <br><br>
    <form action="patientRecord.php"  method="GET">
    <label for="search">Search: </label>
    <input type="text" id="search" name="search" placeholder="Enter patient name">
    <label for="date">Search reservation date</label>
    <input type="date" id="date" name="date">
    <button type="submit">Search</button>
    </form>
    <h1>Upcoming Appointments</h1>
    <?php
    include 'patientRecord.php';
    ?>
    <br><br>
    <h1>Completed Appointments</h1>
        <?php
        include 'completeTransaction.php';
        ?>
    </body>
</html>