<?php
session_start();

// Option A
// session_destroy() is a PHP function that terminates the session and deletes all session data.
session_destroy();

?>

<!DOCTYPE html>
<html>
<head>
    <title>Thank You!</title>
</head>
<body>
    <h1>Thank you!</h1>
    <a href="login.php">Go back to login</a> <br>
</body>
</html>