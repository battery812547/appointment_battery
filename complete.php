<?php
include 'db.php';
session_start();

// Check if the user is logged in
if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

if (isset($_GET['id'])) {
    $appointment_id = $_GET['id'];

    // Move data to the 'complete' table
    $transferSql = "INSERT INTO complete SELECT * FROM appointments WHERE appointment_id = $appointment_id";

    if ($conn->query($transferSql) === TRUE) {
        // Remove the appointment from the 'appointments' table
        $deleteSql = "DELETE FROM appointments WHERE appointment_id = $appointment_id";

        if ($conn->query($deleteSql) === TRUE) {
            header("Location: set_appointment.php");
            exit();
        } else {
            echo "Error removing appointment: " . $conn->error;
        }
    } else {
        echo "Error transferring appointment: " . $conn->error;
    }
} else {
    echo "Invalid appointment ID";
}

$conn->close();
?>