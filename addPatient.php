<?php

include 'db.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
    </head>
    <body>
        <form action="patientInfo.php" method="POST">
        <label for="first_name">First Name:</label>
        <input type="text" id="first_name" name="first_name" pattern="[A-Za-z]+" title="Please enter letters only" required><br><br>

        <label for="last_name">Last Name:</label>
        <input type="text" id="last_name" name="last_name" pattern="[A-Za-z]+" title="Please enter letters only" required><br><br>

        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br><br>

        <label for="contact_number">Contact Number:</label>
        <input type="tel" id="contact_number" name="contact_number" pattern="[0-9]{11}" title="Please enter a valid 11-digit number" required><br><br>

        <label for="age">Age:</label>
        <input type="text" id="age" name="age" pattern="[0-9]+" title="Please enter numbers only" required><br><br>

        <label for="sex">Sex:</label>
        <select id="sex" name="sex">
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="other">Other</option>
        </select><br><br>
        <button type="submit">Submit</button>
        </form>
    </body>
</html>